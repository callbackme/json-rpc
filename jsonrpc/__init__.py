__version = (1, 10, 3)

__version__ = version = '.'.join(map(str, __version))
__project__ = PROJECT = __name__

# Try to use faster alternative before fallback to standard json
try:
    import rapidjson as json
    # monkey patch to provide common interface
    class JSONEncoder(json.Encoder):
        def encode(self, obj):
            return self(obj)

    json.JSONEncoder = JSONEncoder
    json.JSONDecoder = json.Decoder
except ImportError:
    import json

from .manager import JSONRPCResponseManager
from .dispatcher import Dispatcher

dispatcher = Dispatcher()

# lint_ignore=W0611,W0401
